import React, { Component } from "react";
import {
    View,
    Text, TouchableOpacity
} from "react-native";
import { NodeCameraView } from 'react-native-nodemediaclient';
import {
    BaseReduceCompnentClass,
    BaseReduceCompnentRedux
} from "../BaseReduceCompnent";
import { request, PERMISSIONS, RESULTS } from 'react-native-permissions';

export const LiveStream = BaseReduceCompnentRedux(
    (state, stateDefault) => {
        return stateDefault;
    },
    (dispatch, dispatchDefault) => {
        return dispatchDefault;
    },

    class extends BaseReduceCompnentClass {
        constructor(props) {
            super(props);
            this.state = {
                publishBtnTitle: 'Bắt đầu livestream',
                isPublish: false,
            }
        }

        componentDidMount(){
            this.checkingCamera();
        }

        async checkingCamera() {
            let result = await request(PERMISSIONS[Platform.OS.toUpperCase()].CAMERA);
            switch (result) {
                case RESULTS.UNAVAILABLE:
                    displayError('This feature is not available (on this device / in this context)', () => {

                    });
                    break;
                case RESULTS.DENIED:
                    displayError('The permission has not been requested / is denied but requestable', () => {

                    });
                    break;
                case RESULTS.GRANTED:
                    
                    break;
                case RESULTS.BLOCKED:
                    displayError('The permission is denied and not requestable anymore', () => {

                    });
                    break;
            }
        }

        render() {
            return (
                <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                    <NodeCameraView
                        style={{ height: '100%', width: '100%', backgroundColor: 'red' }}
                        ref={(vb) => { this.vb = vb }}
                        outputUrl={"rtmp://103.74.122.94/live/demolivestream2"}
                        camera={{ cameraId: 1, cameraFrontMirror: true }}
                        audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
                        video={{ preset: 12, bitrate: 400000, profile: 1, fps: 15, videoFrontMirror: false }}
                        autopreview={true}
                    />

                    <TouchableOpacity
                        style={{ width: 150, height: 30, backgroundColor: 'blue', borderRadius: 25, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 20, alignSelf: 'center', zIndex: 1 }}
                        onPress={() => {
                            if (this.state.isPublish) {
                                this.setState({ publishBtnTitle: 'Bắt đầu livestream', isPublish: false });
                                this.vb.stop();
                            } else {
                                this.setState({ publishBtnTitle: 'Kết thúc livestream', isPublish: true });
                                this.vb.start();
                            }
                        }}
                        color="#841584"
                    ><Text style={{ color: '#fff', fontSize: 16 }}>{this.state.publishBtnTitle}</Text></TouchableOpacity>
                </View>
            );
        }
    }
);
